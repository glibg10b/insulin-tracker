#include "avr/Eeprom.hpp"
#include "avr/pins.hpp"
#include "avr/power.hpp"
#include "avr/timers.hpp"

// HKD Pro Mini
// Segment pins (top, top left, top right, center, bottom left, bottom right,
// bottom, colon)
#define DISPLAY_SEGMENTS_PINS avr::pC4, avr::pC3, avr::pC0, avr::pB4, avr::pB0,\
	avr::pB3, avr::pB1, avr::pB2
// Section pins (not restricted to four)
#define DISPLAY_SECTIONS_PINS avr::pC5, avr::pC2, avr::pC1, avr::pB5
#include "display.hpp"

#include <avr/interrupt.h>
#include <stdint.h>

constexpr int_fast8_t minInjection{ 1 };
constexpr int_fast8_t maxInjection{ 6 };
constexpr auto& buttonPin{ avr::pD7 };

inline void setupInsulinHistory();
inline void initInterrupts();
constexpr void initDisplay();
inline void sleepLoop();

int main() {
	setupInsulinHistory();

	avr::lowCurrentMode();

	initDisplay();
	initInterrupts();

	sleepLoop();
}

using history_t = uint_fast16_t;

// Each digit represents the amount of units of injected insulin
// Last digit is newest
avr::EEPROMVar<volatile history_t> g_insulinHist{};

consteval auto pow(int_fast64_t x, auto e) {
	for (auto i{ 1 }; i != e; ++i) x *= x;
	return x;
}

template <uint_fast8_t count>
void decimalShiftLeft(auto& x) { x = x % pow(10, count) * 10; }

inline void setupInsulinHistory() {
	g_insulinHist.load();
	display::setStr(g_insulinHist);
	decimalShiftLeft<display::sectionCount-1>(g_insulinHist);
	g_insulinHist.save();
}

inline void initInterrupts() {
	avr::sbis(avr::pcicr, PCIE2);
	avr::sbis(avr::pcmsk2, PCINT23); // PD7 interrupt
	avr::sbis(avr::eecr, EERIE);     // EEPROM ready interrupt
	avr::sbis(avr::timsk0, TOV0, OCIE0A); // t0 OVF and COMPA interrupts
	sei();
}

constexpr void initDisplay() {
	avr::enableTimer1();

	avr::setDDRs<DISPLAY_SEGMENTS_PINS, DISPLAY_SECTIONS_PINS>(
			avr::Pin::output);

	avr::clearTimerCRCache<avr::t0>();

	avr::t0.setWgmStore<avr::t0>(avr::T0::fastPWM, avr::T0::ocr);
	avr::t0.setCSStore<avr::t0>(avr::T0::cs1024);

	avr::flushTimerCRCache<avr::t0>();

	// Found through trial and error
	avr::t0.setOCRA(F_CPU/126984);    // Refresh rate
	avr::t0.setOCRB(F_CPU/126984/10); // Brightness
}

inline void sleepLoop() {
	avr::setSleepMode(avr::sleepMode::idle);
	avr::sleepForever();
}

constexpr auto lastDigit(auto x) { return x % 10ll; }

// Add 1 unit of insulin to x (wraps around at maxInjection)
constexpr auto addUnit(history_t x) {
	if (lastDigit(x) == maxInjection)
		return x - maxInjection + minInjection;
	return ++x;
}

constexpr auto toChar(auto x) { return x + '0'; }

ISR(PCINT2_vect) {
  	if (buttonPin.isLow()) return;

	g_insulinHist = addUnit(g_insulinHist);
	display::setStr(g_insulinHist);

	g_insulinHist.save();
}

ISR(    EE_READY_vect) { avr::EEPROM::isr(); }
ISR(  TIMER0_OVF_vect) { display::display(); }
ISR(TIMER0_COMPB_vect) { display::clear();   }

