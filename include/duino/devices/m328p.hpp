#if !defined(_DUINO_M328P_HPP)
#define      _DUINO_M328P_HPP

#include "avr/devices/m328p.hpp"
#include "avr/Pin.hpp"

namespace arduino {
	constexpr const avr::Pin& pinD0{ avr::pD0 };
	constexpr const avr::Pin& pinD1{ avr::pD1 };
	constexpr const avr::Pin& pinD2{ avr::pD2 };
	constexpr const avr::Pin& pinD3{ avr::pD3 };
	constexpr const avr::Pin& pinD4{ avr::pD4 };
	constexpr const avr::Pin& pinD5{ avr::pD5 };
	constexpr const avr::Pin& pinD6{ avr::pD6 };
	constexpr const avr::Pin& pinD7{ avr::pD7 };
	constexpr const avr::Pin& pinD8{ avr::pB0 };
	constexpr const avr::Pin& pinD9{ avr::pB1 };
	constexpr const avr::Pin& pinD10{ avr::pB2 };
	constexpr const avr::Pin& pinD11{ avr::pB3 };
	constexpr const avr::Pin& pinD12{ avr::pB4 };
	constexpr const avr::Pin& pinD13{ avr::pB5 };
	constexpr const avr::Pin& pinD14{ avr::pB6 };
	constexpr const avr::Pin& pinD15{ avr::pB7 };
} // namespace arduino
#endif // !defined(_DUINO_M328P_HPP)
