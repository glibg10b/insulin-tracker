#if !defined(_AVR_DEVICE_HPP)
#define      _AVR_DEVICE_HPP

#if defined(__AVR_ATmega328__) || defined(__AVR_ATmega328P__)
#	include "avr/devices/m328.hpp"
/*
#elif defined(__AVR_ATmega64__)
#	include "avr/m64.hpp"
#elif defined(__AVR_ATmega64A__)
#	include "avr/m64a.hpp"
#elif defined(__AVR_ATmega640__)
#	include "avr/m640.hpp"
#elif defined(__AVR_ATmega644__)
#	include "avr/m644.hpp"
#elif defined(__AVR_ATmega644A__)
#	include "avr/m644a.hpp"
#elif defined(__AVR_ATmega644P__)
#	include "avr/m644p.hpp"
#elif defined(__AVR_ATmega644PA__)
#	include "avr/m644pa.hpp"
#elif defined(__AVR_ATmega645__) || defined(__AVR_ATmega645A__) || defined(__AVR_ATmega645P__)
#	include "avr/m645.hpp"
#elif defined(__AVR_ATmega6450__) || defined(__AVR_ATmega6450A__) || defined(__AVR_ATmega6450P__)
#	include "avr/m6450.hpp"
#elif defined(__AVR_ATmega649__) || defined(__AVR_ATmega649A__)
#	include "avr/m649.hpp"
#elif defined(__AVR_ATmega6490__) || defined(__AVR_ATmega6490A__) || defined(__AVR_ATmega6490P__)
#	include "avr/m6490.hpp"
#elif defined(__AVR_ATmega649P__)
#	include "avr/m649p.hpp"
#elif defined(__AVR_ATmega64HVE__)
#	include "avr/m64hve.hpp"
#elif defined(__AVR_ATmega64HVE2__)
#	include "avr/m64hve2.hpp"
#elif defined(__AVR_ATmega103__)
#	include "avr/m103.hpp"
#elif defined(__AVR_ATmega32__)
#	include "avr/m32.hpp"
#elif defined(__AVR_ATmega32A__)
#	include "avr/m32a.hpp"
#elif defined(__AVR_ATmega323__)
#	include "avr/m323.hpp"
#elif defined(__AVR_ATmega324P__) || defined(__AVR_ATmega324A__)
#	include "avr/m324.hpp"
#elif defined(__AVR_ATmega324PA__)
#	include "avr/m324pa.hpp"
#elif defined(__AVR_ATmega325__) || defined(__AVR_ATmega325A__)
#	include "avr/m325.hpp"
#elif defined(__AVR_ATmega325P__)
#	include "avr/m325.hpp"
#elif defined(__AVR_ATmega325PA__)
#	include "avr/m325pa.hpp"  
#elif defined(__AVR_ATmega3250__) || defined(__AVR_ATmega3250A__)
#	include "avr/m3250.hpp"
#elif defined(__AVR_ATmega3250P__)
#	include "avr/m3250.hpp"
#elif defined(__AVR_ATmega3250PA__)
#	include "avr/m3250pa.hpp"  
#elif defined(__AVR_ATmega329__) || defined(__AVR_ATmega329A__)
#	include "avr/m329.hpp"
#elif defined(__AVR_ATmega329P__) || defined(__AVR_ATmega329PA__)
#	include "avr/m329.hpp"
#elif defined(__AVR_ATmega3290__) || defined(__AVR_ATmega3290A__)
#	include "avr/m3290.hpp"
#elif defined(__AVR_ATmega3290P__)
#	include "avr/m3290.hpp"
#elif defined(__AVR_ATmega3290PA__)
#	include "avr/m3290pa.hpp" 
#elif defined(__AVR_ATmega32HVB__)
#	include "avr/m32hvb.hpp"
#elif defined(__AVR_ATmega32HVBREVB__)
#	include "avr/m32hvbrevb.hpp"
#elif defined(__AVR_ATmega406__)
#	include "avr/m406.hpp"
#elif defined(__AVR_ATmega16__)
#	include "avr/m16.hpp"
#elif defined(__AVR_ATmega16A__)
#	include "avr/m16a.hpp"
#elif defined(__AVR_ATmega161__)
#	include "avr/m161.hpp"
#elif defined(__AVR_ATmega162__)
#	include "avr/m162.hpp"
#elif defined(__AVR_ATmega163__)
#	include "avr/m163.hpp"
#elif defined(__AVR_ATmega164P__) || defined(__AVR_ATmega164A__)
#	include "avr/m164.hpp"
#elif defined(__AVR_ATmega164PA__)
#	include "avr/m164pa.hpp"
#elif defined(__AVR_ATmega165__)
#	include "avr/m165.hpp"
#elif defined(__AVR_ATmega165A__)
#	include "avr/m165a.hpp"
#elif defined(__AVR_ATmega165P__)
#	include "avr/m165p.hpp"
#elif defined(__AVR_ATmega165PA__)
#	include "avr/m165pa.hpp"
#elif defined(__AVR_ATmega168__)
#	include "avr/m168.hpp"
#elif defined(__AVR_ATmega168A__)
#	include "avr/m168a.hpp"
#elif defined(__AVR_ATmega168P__)
#	include "avr/m168p.hpp"
#elif defined(__AVR_ATmega168PA__)
#	include "avr/m168pa.hpp"
#elif defined(__AVR_ATmega169__) || defined(__AVR_ATmega169A__)
#	include "avr/m169.hpp"
#elif defined(__AVR_ATmega169P__)
#	include "avr/m169p.hpp"
#elif defined(__AVR_ATmega169PA__)
#	include "avr/m169pa.hpp"
#elif defined(__AVR_ATmega8HVA__)
#	include "avr/m8hva.hpp"
#elif defined(__AVR_ATmega16HVA__)
#	include "avr/m16hva.hpp"
#elif defined(__AVR_ATmega16HVA2__)
#	include "avr/m16hva2.hpp"
#elif defined(__AVR_ATmega16HVB__)
#	include "avr/m16hvb.hpp"
#elif defined(__AVR_ATmega16HVBREVB__)
#	include "avr/m16hvbrevb.hpp"
#elif defined(__AVR_ATmega8__)
#	include "avr/m8.hpp"
#elif defined(__AVR_ATmega8A__)
#	include "avr/m8a.hpp"
#elif defined(__AVR_ATmega48__)
#	include "avr/m48.hpp"
#elif defined(__AVR_ATmega48A__)
#	include "avr/m48a.hpp"
#elif defined(__AVR_ATmega48PA__)
#	include "avr/m48pa.hpp"
#elif defined(__AVR_ATmega48PB__)
#	include "avr/m48pb.hpp"
#elif defined(__AVR_ATmega48P__)
#	include "avr/m48p.hpp"
#elif defined(__AVR_ATmega88__)
#	include "avr/m88.hpp"
#elif defined(__AVR_ATmega88A__)
#	include "avr/m88a.hpp"
#elif defined(__AVR_ATmega88P__)
#	include "avr/m88p.hpp"
#elif defined(__AVR_ATmega88PA__)
#	include "avr/m88pa.hpp"
#elif defined(__AVR_ATmega88PB__)
#	include "avr/m88pb.hpp"
#elif defined(__AVR_ATmega8515__)
#	include "avr/m8515.hpp"
#elif defined(__AVR_ATmega8535__)
#	include "avr/m8535.hpp"
#elif defined(__AVR_ATmega8U2__)
#	include "avr/m8u2.hpp"
#elif defined(__AVR_ATmega16M1__)
#	include "avr/m16m1.hpp"
#elif defined(__AVR_ATmega16U2__)
#	include "avr/m16u2.hpp"
#elif defined(__AVR_ATmega16U4__)
#	include "avr/m16u4.hpp"
#elif defined(__AVR_ATmega32C1__)
#	include "avr/m32c1.hpp"
#elif defined(__AVR_ATmega32M1__)
#	include "avr/m32m1.hpp"
#elif defined(__AVR_ATmega32U2__)
#	include "avr/m32u2.hpp"
#elif defined(__AVR_ATmega32U4__)
#	include "avr/m32u4.hpp"
#elif defined(__AVR_ATmega32U6__)
#	include "avr/m32u6.hpp"
#elif defined(__AVR_ATmega64C1__)
#	include "avr/m64c1.hpp"
#elif defined(__AVR_ATmega64M1__)
#	include "avr/m64m1.hpp"
#elif defined(__AVR_ATmega128__)
#	include "avr/m128.hpp"
#elif defined(__AVR_ATmega128A__)
#	include "avr/m128a.hpp"
#elif defined(__AVR_ATmega1280__)
#	include "avr/m1280.hpp"
#elif defined(__AVR_ATmega1281__)
#	include "avr/m1281.hpp"
#elif defined(__AVR_ATmega1284__)
#	include "avr/m1284.hpp"
#elif defined(__AVR_ATmega1284P__)
#	include "avr/m1284p.hpp"
#elif defined(__AVR_ATmega128RFA1__)
#	include "avr/m128rfa1.hpp"
#elif defined(__AVR_ATmega1284RFR2__)
#	include "avr/m1284rfr2.hpp"
#elif defined(__AVR_ATmega128RFR2__)
#	include "avr/m128rfr2.hpp"
#elif defined(__AVR_ATmega2564RFR2__)
#	include "avr/m2564rfr2.hpp"
#elif defined(__AVR_ATmega256RFR2__)
#	include "avr/m256rfr2.hpp"
#elif defined(__AVR_ATmega2560__)
#	include "avr/m2560.hpp"
#elif defined(__AVR_ATmega2561__)
#	include "avr/m2561.hpp"
#elif defined(__AVR_ATxmega16A4__)
#	include "avr/x16a4.hpp"
#elif defined(__AVR_ATxmega16A4U__)
#	include "avr/x16a4u.hpp"
#elif defined(__AVR_ATxmega16C4__)
#	include "avr/x16c4.hpp"
#elif defined(__AVR_ATxmega16D4__)
#	include "avr/x16d4.hpp"
#elif defined(__AVR_ATxmega32A4__)
#	include "avr/x32a4.hpp"
#elif defined(__AVR_ATxmega32A4U__)
#	include "avr/x32a4u.hpp"
#elif defined(__AVR_ATxmega32C3__)
#	include "avr/x32c3.hpp"
#elif defined(__AVR_ATxmega32C4__)
#	include "avr/x32c4.hpp"
#elif defined(__AVR_ATxmega32D3__)
#	include "avr/x32d3.hpp"
#elif defined(__AVR_ATxmega32D4__)
#	include "avr/x32d4.hpp"
#elif defined(__AVR_ATxmega32E5__)
#	include "avr/x32e5.hpp"
#elif defined(__AVR_ATxmega64A1__)
#	include "avr/x64a1.hpp"
#elif defined(__AVR_ATxmega64A1U__)
#	include "avr/x64a1u.hpp"
#elif defined(__AVR_ATxmega64A3__)
#	include "avr/x64a3.hpp"
#elif defined(__AVR_ATxmega64A3U__)
#	include "avr/x64a3u.hpp"
#elif defined(__AVR_ATxmega64A4U__)
#	include "avr/x64a4u.hpp"
#elif defined(__AVR_ATxmega64B1__)
#	include "avr/x64b1.hpp"
#elif defined(__AVR_ATxmega64B3__)
#	include "avr/x64b3.hpp"
#elif defined(__AVR_ATxmega64C3__)
#	include "avr/x64c3.hpp"
#elif defined(__AVR_ATxmega64D3__)
#	include "avr/x64d3.hpp"
#elif defined(__AVR_ATxmega64D4__)
#	include "avr/x64d4.hpp"
#elif defined(__AVR_ATxmega128A1__)
#	include "avr/x128a1.hpp"
#elif defined(__AVR_ATxmega128A1U__)
#	include "avr/x128a1u.hpp"
#elif defined(__AVR_ATxmega128A4U__)
#	include "avr/x128a4u.hpp"
#elif defined(__AVR_ATxmega128A3__)
#	include "avr/x128a3.hpp"
#elif defined(__AVR_ATxmega128A3U__)
#	include "avr/x128a3u.hpp"
#elif defined(__AVR_ATxmega128B1__)
#	include "avr/x128b1.hpp"
#elif defined(__AVR_ATxmega128B3__)
#	include "avr/x128b3.hpp"
#elif defined(__AVR_ATxmega128C3__)
#	include "avr/x128c3.hpp"
#elif defined(__AVR_ATxmega128D3__)
#	include "avr/x128d3.hpp"
#elif defined(__AVR_ATxmega128D4__)
#	include "avr/x128d4.hpp"
#elif defined(__AVR_ATxmega192A3__)
#	include "avr/x192a3.hpp"
#elif defined(__AVR_ATxmega192A3U__)
#	include "avr/x192a3u.hpp"
#elif defined(__AVR_ATxmega192C3__)
#	include "avr/x192c3.hpp"
#elif defined(__AVR_ATxmega192D3__)
#	include "avr/x192d3.hpp"
#elif defined(__AVR_ATxmega256A3__)
#	include "avr/x256a3.hpp"
#elif defined(__AVR_ATxmega256A3U__)
#	include "avr/x256a3u.hpp"
#elif defined(__AVR_ATxmega256A3B__)
#	include "avr/x256a3b.hpp"
#elif defined(__AVR_ATxmega256A3BU__)
#	include "avr/x256a3bu.hpp"
#elif defined(__AVR_ATxmega256C3__)
#	include "avr/x256c3.hpp"
#elif defined(__AVR_ATxmega256D3__)
#	include "avr/x256d3.hpp"
#elif defined(__AVR_ATxmega384C3__)
#	include "avr/x384c3.hpp"
#elif defined(__AVR_ATxmega384D3__)
#	include "avr/x384d3.hpp"
#elif defined(__AVR_AT94K__)
#	include "avr/at94k.hpp"
#elif defined(__AVR_AT43USB320__)
#	include "avr/43u32x.hpp"
#elif defined(__AVR_AT43USB355__)
#	include "avr/43u35x.hpp"
#elif defined(__AVR_AT76C711__)
#	include "avr/76c711.hpp"
#elif defined(__AVR_AT86RF401__)
#	include "avr/86r401.hpp"
#elif defined(__AVR_AT90PWM1__)
#	include "avr/90pwm1.hpp"
#elif defined(__AVR_AT90PWM2__)
#	include "avr/90pwmx.hpp"
#elif defined(__AVR_AT90PWM2B__)
#	include "avr/90pwm2b.hpp"
#elif defined(__AVR_AT90PWM3__)
#	include "avr/90pwmx.hpp"
#elif defined(__AVR_AT90PWM3B__)
#	include "avr/90pwm3b.hpp"
#elif defined(__AVR_AT90PWM216__)
#	include "avr/90pwm216.hpp"
#elif defined(__AVR_AT90PWM316__)
#	include "avr/90pwm316.hpp"
#elif defined(__AVR_AT90PWM161__)
#	include "avr/90pwm161.hpp"
#elif defined(__AVR_AT90PWM81__)
#	include "avr/90pwm81.hpp"
#elif defined(__AVR_AT90CAN32__)
#	include "avr/can32.hpp"
#elif defined(__AVR_AT90CAN64__)
#	include "avr/can64.hpp"
#elif defined(__AVR_AT90CAN128__)
#	include "avr/can128.hpp"
#elif defined(__AVR_AT90USB82__)
#	include "avr/usb82.hpp"
#elif defined(__AVR_AT90USB162__)
#	include "avr/usb162.hpp"
#elif defined(__AVR_AT90USB646__)
#	include "avr/usb646.hpp"
#elif defined(__AVR_AT90USB647__)
#	include "avr/usb647.hpp"
#elif defined(__AVR_AT90USB1286__)
#	include "avr/usb1286.hpp"
#elif defined(__AVR_AT90USB1287__)
#	include "avr/usb1287.hpp"
#elif defined(__AVR_ATmega644RFR2__)
#	include "avr/m644rfr2.hpp"
#elif defined(__AVR_ATmega64RFR2__)
#	include "avr/m64rfr2.hpp"
#elif defined(__AVR_AT90S8535__)
#	include "avr/8535.hpp"
#elif defined(__AVR_AT90C8534__)
#	include "avr/8534.hpp"
#elif defined(__AVR_AT90S8515__)
#	include "avr/8515.hpp"
#elif defined(__AVR_AT90S4434__)
#	include "avr/4434.hpp"
#elif defined(__AVR_AT90S4433__)
#	include "avr/4433.hpp"
#elif defined(__AVR_AT90S4414__)
#	include "avr/4414.hpp"
#elif defined(__AVR_ATtiny22__)
#	include "avr/tn22.hpp"
#elif defined(__AVR_ATtiny26__)
#	include "avr/tn26.hpp"
#elif defined(__AVR_AT90S2343__)
#	include "avr/2343.hpp"
#elif defined(__AVR_AT90S2333__)
#	include "avr/2333.hpp"
#elif defined(__AVR_AT90S2323__)
#	include "avr/2323.hpp"
#elif defined(__AVR_AT90S2313__)
#	include "avr/2313.hpp"
#elif defined(__AVR_ATtiny4__)
#	include "avr/tn4.hpp"
#elif defined(__AVR_ATtiny5__)
#	include "avr/tn5.hpp"
#elif defined(__AVR_ATtiny9__)
#	include "avr/tn9.hpp"
#elif defined(__AVR_ATtiny10__)
#	include "avr/tn10.hpp"
#elif defined(__AVR_ATtiny20__)
#	include "avr/tn20.hpp"
#elif defined(__AVR_ATtiny40__)
#	include "avr/tn40.hpp"
#elif defined(__AVR_ATtiny2313__)
#	include "avr/tn2313.hpp"
#elif defined(__AVR_ATtiny2313A__)
#	include "avr/tn2313a.hpp"
#elif defined(__AVR_ATtiny13__)
#	include "avr/tn13.hpp"
#elif defined(__AVR_ATtiny13A__)
#	include "avr/tn13a.hpp"
#elif defined(__AVR_ATtiny25__)
#	include "avr/tn25.hpp"
#elif defined(__AVR_ATtiny4313__)
#	include "avr/tn4313.hpp"
#elif defined(__AVR_ATtiny45__)
#	include "avr/tn45.hpp"
#elif defined(__AVR_ATtiny85__)
#	include "avr/tn85.hpp"
#elif defined(__AVR_ATtiny24__)
#	include "avr/tn24.hpp"
#elif defined(__AVR_ATtiny24A__)
#	include "avr/tn24a.hpp"
#elif defined(__AVR_ATtiny44__)
#	include "avr/tn44.hpp"
#elif defined(__AVR_ATtiny44A__)
#	include "avr/tn44a.hpp"
#elif defined(__AVR_ATtiny441__)
#	include "avr/tn441.hpp"
#elif defined(__AVR_ATtiny84__)
#	include "avr/tn84.hpp"
#elif defined(__AVR_ATtiny84A__)
#	include "avr/tn84a.hpp" 
#elif defined(__AVR_ATtiny841__)
#	include "avr/tn841.hpp" 
#elif defined(__AVR_ATtiny261__)
#	include "avr/tn261.hpp"
#elif defined(__AVR_ATtiny261A__)
#	include "avr/tn261a.hpp"
#elif defined(__AVR_ATtiny461__)
#	include "avr/tn461.hpp"
#elif defined(__AVR_ATtiny461A__)
#	include "avr/tn461a.hpp"
#elif defined(__AVR_ATtiny861__)
#	include "avr/tn861.hpp"
#elif defined(__AVR_ATtiny861A__)
#	include "avr/tn861a.hpp"
#elif defined(__AVR_ATtiny43U__)
#	include "avr/tn43u.hpp"
#elif defined(__AVR_ATtiny48__)
#	include "avr/tn48.hpp"
#elif defined(__AVR_ATtiny88__)
#	include "avr/tn88.hpp"
#elif defined(__AVR_ATtiny828__)
#	include "avr/tn828.hpp"
#elif defined(__AVR_ATtiny87__)
#	include "avr/tn87.hpp"
#elif defined(__AVR_ATtiny167__)
#	include "avr/tn167.hpp"
#elif defined(__AVR_ATtiny1634__)
#	include "avr/tn1634.hpp"
#elif defined(__AVR_AT90SCR100__)
#	include "avr/90scr100.hpp"
#elif defined(__AVR_ATxmega8E5__)
#	include "avr/x8e5.hpp"
#elif defined(__AVR_ATA5702M322__)
#	include "avr/a5702m322.hpp"
#elif defined(__AVR_ATA5782__)
#	include "avr/a5782.hpp"
#elif defined(__AVR_ATA5790__)
#	include "avr/a5790.hpp"
#elif defined(__AVR_ATA5790N__)
#	include "avr/a5790n.hpp"
#elif defined(__AVR_ATA5831__)
#	include "avr/a5831.hpp"
#elif defined(__AVR_ATA5272__)
#	include "avr/a5272.hpp"
#elif defined(__AVR_ATA5505__)
#	include "avr/a5505.hpp"
#elif defined(__AVR_ATA5795__)
#	include "avr/a5795.hpp"
#elif defined(__AVR_ATA6285__)
#	include "avr/a6285.hpp"
#elif defined(__AVR_ATA6286__)
#	include "avr/a6286.hpp"
#elif defined(__AVR_ATA6289__)
#	include "avr/a6289.hpp"
#elif defined(__AVR_ATA6612C__)
#	include "avr/a6612c.hpp"
#elif defined(__AVR_ATA6613C__)
#	include "avr/a6613c.hpp"
#elif defined(__AVR_ATA6614Q__)
#	include "avr/a6614q.hpp"
#elif defined(__AVR_ATA6616C__)
#	include "avr/a6616c.hpp"
#elif defined(__AVR_ATA6617C__)
#	include "avr/a6617c.hpp"
#elif defined(__AVR_ATA664251__)
#	include "avr/a664251.hpp"
// avr1: the following only supported for assembler programs
#elif defined(__AVR_ATtiny28__)
#	include "avr/tn28.hpp"
#elif defined(__AVR_AT90S1200__)
#	include "avr/1200.hpp"
#elif defined(__AVR_ATtiny15__)
#	include "avr/tn15.hpp"
#elif defined(__AVR_ATtiny12__)
#	include "avr/tn12.hpp"
#elif defined(__AVR_ATtiny11__)
#	include "avr/tn11.hpp"
#elif defined(__AVR_M3000__)
#	include "avr/m3000.hpp"
*/
#else
#	error Unsupported device
#endif

#endif // !defined(_AVR_DEVICE_HPP)
