#if !defined(_AVR_PORT_HPP)
#define      _AVR_PORT_HPP

#include "avr/Cache.hpp"
#include "avr/Register.hpp"
#include "avr/types.hpp"

#include <avr/io.h>

namespace avr {

class Port {
public:
	Port() = delete;
	Port(const Port&) = delete;
	Port& operator=(const Port&) = delete;

	consteval Port(const Register& reg, const Register& ddr,
			const Register& pin)
		: m_reg{ reg }, m_ddr{ ddr }, m_pin{ pin }
	{}

	constexpr auto& reg() const { return m_reg; }
	constexpr auto& ddr() const { return m_ddr; }
	constexpr auto& pin() const { return m_pin; }
	constexpr operator const Register&() { return reg(); }
private:
	const Register& m_reg;
	const Register& m_ddr;
	const Register& m_pin;
};

} // namespace arduino

#endif // !defined(_AVR_PORT_HPP)
