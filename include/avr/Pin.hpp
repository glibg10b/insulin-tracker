#if !defined(_AVR_PIN_HPP)
#define      _AVR_PIN_HPP

#include "avr/Port.hpp"
#include "avr/Timer.hpp"
#include "avr/bits.hpp"
#include "avr/types.hpp"

#include <avr/io.h>
#include <stdint.h>

namespace avr {

class Pin {
public:
	Pin() = delete;
	Pin(const Pin&) = delete;
	Pin& operator=(const Pin&) = delete;

	enum Level{ low, high };
	// Data direction
	enum Ddr{ input, output };

	consteval Pin(const Port& port, bit_t bit, const Register* didr=nullptr,
			uint_fast8_t didbit=UINT_FAST8_MAX)
		: m_port{ port }
		, m_bit{ bit }
		, m_didr{ didr }
		, m_didbit{ didbit }
	{}
	consteval operator const Port&() { return m_port; }
	consteval const auto& port() const { return m_port; }
	constexpr auto bit() const { return m_bit; }
	constexpr auto mask() const { return _BV(bit()); }

	// Assumes input buffer can be turned off
	consteval const auto& didr() const { return *m_didr; }
	consteval auto didbit() const { return m_didbit; }

	// Digital output level
	// Assumes DDR is output
	void setLevel(Level level) const {
		if (level == low) sbyte(m_port.reg(), mask());
		else              cbyte(m_port.reg(), mask());
	}

	// Input/output pin modes
	void setDdr(Ddr mode) const {
		if (mode == input) sbyte(m_port.ddr(), mask());
		else               cbyte(m_port.ddr(), mask());
	}

	// Turn the pullup resistor on/off
	// Assumes DDR is input
	void setPullup(bool pullup) const {
		if (pullup) sbyte(m_port.reg(), mask());
		else        cbyte(m_port.reg(), mask());
	}

	constexpr bool isHigh() const { return m_port.pin() & mask(); }
	constexpr bool isLow() const { return !(m_port.pin() & mask()); }
private:
	const Port& m_port;
	const bit_t m_bit{};
	const Register* m_didr{ nullptr };
	const uint_fast8_t m_didbit{};
};

} // namespace avr

#endif // !defined(_AVR_PIN_HPP)
