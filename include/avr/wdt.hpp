#if !defined(_DUINO_GENERAL_HPP_1657032197)
#define      _DUINO_GENERAL_HPP_1657032197

#include <avr/wdt.h>
#include <avr/io.h>

namespace arduino {
#if defined(DUINO_WATCHDOG_INIT)
	void _initWdt(void) __attribute__((naked))
		__attribute__((section(".init3")));

	void _initWdt(void)
	// https://www.nongnu.org/avr-libc/user-manual/FAQ.html#faq_softreset
	{
		MCUSR = 0;
		wdt_disable();
	}

	/** @brief Reset the Arduino */
	inline void reset() {
		wdt_enable(WDTO_15MS);
		while (true);
	}
#endif // defined(DUINO_WATCHDOG_INIT)
} // namespace arduino

#endif // !defined(_DUINO_GENERAL_HPP_1657032197)
