// Functions for interacting with the on-chip ADC(s) and analog comparator(s)
#if !defined(_AVR_ANALOG_HPP)
#define      _AVR_ANALOG_HPP

#include "avr/device.hpp"
#include "avr/bits.hpp"

namespace avr {

constexpr void setADCPower(bool on) {
	if (on) cbis(acsr, ACD); else sbis(acsr, ACD);
}

} // namespace avr

#endif // !defined(_AVR_ANALOG_HPP)
