#if !defined(_AVR_TYPES_HPP)
#define      _AVR_TYPES_HPP

#include <stdint.h>

namespace avr {

using reg_t   = uint8_t;
using reg16_t = uint16_t;
using bit_t   = uint_fast8_t;

} // namespace arduino
#endif // !defined(_AVR_TYPES_HPP)
