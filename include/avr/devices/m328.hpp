#if !defined(_AVR_M328P_HPP)
#define      _AVR_M328P_HPP

#include "avr/bits.hpp"
#include "avr/Cache.hpp"
#include "avr/Port.hpp"
#include "avr/Pin.hpp"
#include "avr/Register.hpp"
#include "avr/Register16.hpp"
#include "avr/Timer.hpp"
#include "libministdcxx/functional"

#include <avr/io.h>
#include <avr/sfr_defs.h>
#include <stdint.h>

namespace avr {

////////////////////////////////// Registers ///////////////////////////////////

constexpr Register   pinb{  PINB  };
constexpr Register   pinc{  PINC  };
constexpr Register   pind{  PIND  };
constexpr Register   ddrb{  DDRB  };
constexpr Register   ddrc{  DDRC  };
constexpr Register   ddrd{  DDRD  };
constexpr Register   portb{ PORTB };
constexpr Register   portc{ PORTC };
constexpr Register   portd{ PORTD };

constexpr Register   tcnt0{  TCNT0 };
constexpr Register   ocr0a{  OCR0A };
constexpr Register   ocr0b{  OCR0B };
constexpr Register   tccr0a{ TCCR0A };
constexpr Register   tccr0b{ TCCR0B };
const     Register   timsk0{ TIMSK0 };
const     Register16 tcnt1{  TCNT1 };
const     Register16 icr1{   ICR1  };
const     Register16 ocr1a{  OCR1A };
const     Register16 ocr1b{  OCR1B };
const     Register   tccr1a{ TCCR1A };
const     Register   tccr1b{ TCCR1B };
const     Register   tccr1c{ TCCR1C };
const     Register   timsk1{ TIMSK1 };
const     Register   tcnt2{  TCNT2 };
const     Register   ocr2a{  OCR2A };
const     Register   ocr2b{  OCR2B };
const     Register   tccr2a{ TCCR2A };
const     Register   tccr2b{ TCCR2B };
const     Register   timsk2{ TIMSK2 };

constexpr Register eecr{ EEDR };
// "EEAR8 is an unused bit in ATmega328P and must always be written to zero"
constexpr Register eear{ EEARL };
const     Register eedr{ EEDR };

constexpr Register acsr { ACSR  };
const     Register didr0{ DIDR0 };
const     Register didr1{ DIDR1 };

constexpr Register mcucr{ MCUCR };

const Register pcicr { PCICR  };
const Register pcmsk0{ PCMSK0 };
const Register pcmsk1{ PCMSK1 };
const Register pcmsk2{ PCMSK2 };

constexpr Register smcr { SMCR };

const Register prr{ PRR };

//////////////////////////////////// Ports /////////////////////////////////////

enum { _pb, _pc, _pd, maxPorts };
constexpr auto portCount{ maxPorts };

constexpr Port pB{ portb, ddrb, pinb };
constexpr Port pC{ portc, ddrc, pinc };
constexpr Port pD{ portd, ddrd, pind };

constexpr const ministd::reference_wrapper<const avr::Port> ports[maxPorts]{
	pB, pC, pD
};

//////////////////////////////////// Timers ////////////////////////////////////

enum { _t0, _t1, _t2, maxTimers };
constexpr Timer t0{ TIMER0_COMPA_vect_num, TIMER0_COMPB_vect_num,
	TIMER0_OVF_vect_num, tcnt0, ocr0a, ocr0b, tccr0a, tccr0b };
constexpr Timer t1{ TIMER0_COMPA_vect_num, TIMER0_COMPB_vect_num,
	TIMER0_OVF_vect_num, tcnt1, ocr1a, ocr1b, tccr1a, tccr1b, &tccr1c,
	&icr1 };
constexpr Timer t2{ TIMER0_COMPA_vect_num, TIMER0_COMPB_vect_num,
	TIMER0_OVF_vect_num, tcnt2, ocr2a, ocr2b, tccr2a, tccr2b };

template <const auto& Timer>
// Placeholder
constexpr const ministd::reference_wrapper<const Register> timerCRs[]{ prr };

template <>
constexpr const ministd::reference_wrapper<const Register>
timerCRs<t0>[]{ tccr0a, tccr0b };

template <>
constexpr const ministd::reference_wrapper<const Register>
timerCRs<t1>[]{ tccr1a, tccr1b, tccr1c };

template <>
constexpr const ministd::reference_wrapper<const Register>
timerCRs<t2>[]{ tccr2a, tccr2b };

template <const auto& Timer>
constexpr auto crCount{ sizeof(timerCRs<Timer>)/sizeof(timerCRs<Timer>[0]) };

using T0 = decltype(t0);
using T1 = decltype(t1);
using T2 = decltype(t2);

// If constexpr control flow reaches a call to fail(const char*), this will fail
// (intentionally)
constexpr void fail(auto msg){ msg = msg + 1; };

template <>
template <>
constexpr void T0::setComAStore<t0>(T0::Com comA) const {
	Cache<t0.m_crA>::mask() |= mask(COM0A0, COM0A1);

	auto& store{ Cache<t0.m_crA>::store() };
	switch (comA)
	{
	case none:                                   break;
	case toggle:    sbis(store,         COM0A0); break;
	case normalCOM: sbis(store, COM0A1        ); break;
	case inverse:   sbis(store, COM0A1, COM0A0); break;
	default: fail("Unsupported value for COM0A");
	}
}

template <>
template <>
constexpr void T0::setComBStore<t0>(T0::Com comB) const {
	Cache<t0.m_crA>::mask() |= mask(COM0B0, COM0B1);

	auto& store{ Cache<t0.m_crA>::store() };
	switch (comB)
	{
	case none:                                   break;
	case toggle:    sbis(store,         COM0B0); break;
	case normalCOM: sbis(store, COM0B1        ); break;
	case inverse:   sbis(store, COM0B1, COM0B0); break;
	default: fail("Unsupported value for COM0B");
	}
}
template <> template <>
constexpr void T0::setWgmStore<t0>(T0::Wgm wgm, T0::WgTop top) const {
	Cache<t0.m_crA>::mask() |= mask(WGM00, WGM01);
	Cache<t0.m_crB>::mask() |= mask(WGM02);

	auto& storeA{ Cache<t0.m_crA>::store() };
	auto& storeB{ Cache<t0.m_crB>::store() };
	switch (wgm)
	{
	case normalWgm:
		switch (top)
		{
		case maxTop: break;
		default: fail("Normal mode: Unsupported TOP");
		}
		break;
	case phaseCorrect:
		switch (top)
		{
		case maxTop:
		case b8:  sbis(storeA, WGM00);                      break;
		case ocr: sbis(storeA, WGM00); sbis(storeB, WGM02); break;
		default: fail("Phase correct PWM mode: Unsuppported TOP");
		}
		break;
	case ctc:
		switch (top)
		{
		case ocr: sbis(storeA, WGM01); break;
		default: fail("CTC mode: Unsupported TOP");
		}
		break;
	case fastPWM:
		switch (top)
		{
		case maxTop:
		case b8:  sbis(storeA, WGM01, WGM00);                      break;
		case ocr: sbis(storeA, WGM01, WGM00); sbis(storeB, WGM02); break;
		default: fail("Fast PWM mode: Unsuppported TOP");
		}
		break;
	default: fail("Unsupported wave generation mode");
	}
}

template <> template <>
constexpr void T0::setCSStore<t0>(T0::Cs cs) const {
	sbis(Cache<t0.m_crB>::mask(), CS02, CS01, CS00);

	auto& store{ Cache<t0.m_crB>::store() };
	store |= cs;
}
template <>
template <>
constexpr void T1::setComAStore<t1>(T1::Com comA) const {
	Cache<t1.m_crA>::mask() |= mask(COM0A0, COM0A1);

	auto& store{ Cache<t1.m_crA>::store() };
	switch (comA)
	{
	case none:                                   break;
	case toggle:    sbis(store,         COM1A0); break;
	case normalCOM: sbis(store, COM1A1        ); break;
	case inverse:   sbis(store, COM1A1, COM1A0); break;
	default: fail("Unsupported value for COM1A");
	}
}

template <>
template <>
constexpr void T1::setComBStore<t1>(T1::Com comB) const {
	Cache<t1.m_crA>::mask() |= mask(COM1B0, COM1B1);

	auto& store{ Cache<t1.m_crA>::store() };
	switch (comB)
	{
	case none:                                   break;
	case toggle:    sbis(store,         COM1B0); break;
	case normalCOM: sbis(store, COM1B1        ); break;
	case inverse:   sbis(store, COM1B1, COM1B0); break;
	default: fail("Unsupported value for COM1B");
	}
}
template <> template <>
constexpr void T1::setWgmStore<t1>(T1::Wgm wgm, T1::WgTop top) const {
	Cache<t1.m_crA>::mask() |= mask(WGM10, WGM11);
	Cache<t1.m_crB>::mask() |= mask(WGM12, WGM13);

	auto& storeA{ Cache<t1.m_crA>::store() };
	auto& storeB{ Cache<t1.m_crB>::store() };
	switch (wgm)
	{
	case normalWgm:
		switch (top)
		{
		case maxTop: break;
		default: fail("Normal mode: Unsupported TOP");
		}
		break;
	case phaseCorrect:
		switch (top)
		{
		case b8:  sbis(storeA,        WGM10);                     break;
		case b9:  sbis(storeA, WGM11       );                     break;
		case b10: sbis(storeA, WGM11, WGM10);                     break;
		case icr: sbis(storeA, WGM11       ); sbis(storeB, WGM13);break;
		case ocr: sbis(storeA, WGM11, WGM10); sbis(storeB, WGM13);break;
		default: fail("Phase correct PWM mode: Unsuppported TOP");
		}
		break;
	case ctc:
		switch (top)
		{
		case ocr: sbis(storeB, WGM12); break;
		default: fail("CTC mode: Unsupported TOP");
		}
		break;
	case fastPWM:
		switch (top)
		{
		case b8:  sbis(storeA,        WGM10); sbis(storeB,        WGM12); break;
		case b9:  sbis(storeA, WGM11       ); sbis(storeB,        WGM12); break;
		case b10: sbis(storeA, WGM11, WGM10); sbis(storeB,        WGM12); break;
		case icr: sbis(storeA, WGM11       ); sbis(storeB, WGM13, WGM12); break;
		case ocr: sbis(storeA, WGM11, WGM10); sbis(storeB, WGM13, WGM12); break;
		default: fail("Fast PWM mode: Unsuppported TOP");
		}
		break;
	case phaseAndFrequencyCorrectPWM:
		switch (top)
		{
		case icr:                      sbis(storeB, WGM13); break;
		case ocr: sbis(storeA, WGM10); sbis(storeB, WGM13); break;
		default: fail(
			"Phase and frequency correct PWM mode: Unsuppported TOP"
			);
		}
		break;
	default: fail("Unsupported wave generation mode");
	}
}

template <> template <>
constexpr void T1::setCSStore<t1>(T1::Cs cs) const {
	sbis(Cache<t1.m_crB>::mask(), CS12, CS11, CS10);

	auto& store{ Cache<t1.m_crB>::store() };
	store |= cs;
}

///////////////////////////////////// Pins /////////////////////////////////////

constexpr auto pinCount{ 23 };

constexpr Pin pB0{ pB, PB0 };
constexpr Pin pB1{ pB, PB1 };
constexpr Pin pB2{ pB, PB2 };
constexpr Pin pB3{ pB, PB3 };
constexpr Pin pB4{ pB, PB4 };
constexpr Pin pB5{ pB, PB5 };
constexpr Pin pB6{ pB, PB6 };
constexpr Pin pB7{ pB, PB7 };
constexpr Pin pC0{ pC, PC0, &didr0, ADC1D };
constexpr Pin pC1{ pC, PC1, &didr0, ADC2D };
constexpr Pin pC2{ pC, PC2, &didr0, ADC3D };
constexpr Pin pC3{ pC, PC3, &didr0, ADC4D };
constexpr Pin pC4{ pC, PC4, &didr0, ADC5D };
constexpr Pin pC5{ pC, PC5 };
constexpr Pin pC6{ pC, PC6 };
constexpr Pin pD0{ pD, PD0 };
constexpr Pin pD1{ pD, PD1 };
constexpr Pin pD2{ pD, PD2 };
constexpr Pin pD3{ pD, PD3 };
constexpr Pin pD4{ pD, PD4 };
constexpr Pin pD5{ pD, PD5 };
constexpr Pin pD6{ pD, PD6, &didr1, AIN0D };
constexpr Pin pD7{ pD, PD7, &didr1, AIN1D };

constexpr auto bufferedInputCount{ 7 };

// Pins with digital input buffers that can be turned off
constexpr const ministd::reference_wrapper<const avr::Pin>
bufferedInputs[bufferedInputCount]{ pC0, pC1, pC2, pC3, pC4, pD6, pD7 };

} // namespace avr

#endif // !defined(_AVR_M328P_HPP)
