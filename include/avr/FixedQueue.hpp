#if !defined(_AVR_FIXEDQUEUE_HPP)
#define      _AVR_FIXEDQUEUE_HPP

#include <stddef.h>

namespace avr {

template <typename T, size_t N>
class FixedQueue {
public:
	FixedQueue() = default;

	constexpr bool full   () { return plusOne(m_front) == m_back; }
	constexpr bool notFull() { return plusOne(m_front) != m_back; }
	constexpr bool empty   () { return m_front == m_back; }
	constexpr bool notEmpty() { return m_front != m_back; }

	constexpr void push(const T& obj) {
		m_back = plusOne(m_back);
		*m_back = obj;
	}

	constexpr T pop() {
		m_front = plusOne(m_front);
		return *m_front;
	}

private:
	T* plusOne(T* ptr) { 
		if (ptr + 1 == m_data + N) return m_data;
		return ptr + 1;
	}

	T m_data[N];

	// Last pushed element in queue
	T* m_back{ m_data };

	// 1 before first element in queue (i.e. next to pop)
	T* m_front{ m_data };
};

} // namespace avr

#endif // !_AVR_FIXEDQUEUE_HPP
