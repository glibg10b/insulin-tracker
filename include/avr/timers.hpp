// Functions for interacting with the on-chip timer(s)
#if !defined(_AVR_TIMERS_HPP)
#define      _AVR_TIMERS_HPP

#include "avr/Cache.hpp"
#include "avr/device.hpp"
#include "libministdcxx/utility"

namespace avr {

template <const auto& Timer, size_t... Idx>
constexpr void _clearTimerCRCacheHelper(ministd::index_sequence<Idx...> const&)
{
	((Cache<timerCRs<Timer>[Idx].get()>::store() = 0b0), ...);
}

template <const auto& Timer>
constexpr void clearTimerCRCache() {
	_clearTimerCRCacheHelper<Timer>(
			ministd::make_index_sequence<crCount<Timer>>{});
}

template <const auto& Timer, size_t... Idx>
constexpr void _flushTimerCRCacheHelper(ministd::index_sequence<Idx...> const&)
{
	(Cache<timerCRs<Timer>[Idx].get()>::flush(), ...);
}

template <const auto& Timer>
constexpr void flushTimerCRCache() {
	_flushTimerCRCacheHelper<Timer>(
			ministd::make_index_sequence<crCount<Timer>>{});
}

} // namespace avr
#endif // !defined(_AVR_TIMERS_HPP)

