#if !defined(_AVR_BITS_HPP)
#define      _AVR_BITS_HPP

#include <avr/sfr_defs.h>
#include <stddef.h>
#include <stdint.h>

namespace avr {
	template <typename... BitPos>
	constexpr auto mask(BitPos... pos) { return (_BV(pos) | ...); }

	template <typename T, typename... BitPos>
	constexpr T mask(BitPos... pos) { return (_BV(pos) | ...); }

	template <typename T = int_fast8_t>
	constexpr T mask() { return 0b0; }

	inline constexpr void cbyte(auto& x, auto bits) { x &= ~bits; }
	inline constexpr void sbyte(auto& x, auto bits) { x |= bits; }

	// [sc]bi are already used by the Arduino Core library, and since
	// they're macros, they disregard namespaces >:(
	
	template <typename... BitPos>
	inline constexpr void cbis(auto& x, BitPos... pos) {
		cbyte(x, mask(pos...));
	}
	template <typename... BitPos>
	inline constexpr void sbis(auto& x, BitPos... pos) {
		sbyte(x, mask(pos...));
	}
}

#endif // !defined(_AVR_BITS_HPP)
