#if !defined(_AVR_EEPROM_HPP)
#define      _AVR_EEPROM_HPP

#include "avr/FixedQueue.hpp"
#include "avr/bits.hpp"
#include "avr/device.hpp"
#include "avr/sleep.hpp"
#include "avr/types.hpp"

#include <avr/interrupt.h>
#include <stdint.h>

#if !defined(AVR_EEPROM_QUEUE_SIZE)
#define      AVR_EEPROM_QUEUE_SIZE 5
#endif // !defined(AVR_EEPROM_QUEUE_SIZE)

namespace avr {

// A byte and address that should be written to the EEPROM
class EEPROMJob{
public:
	// Handle this job (i.e. write byte at address)
	inline void handle() volatile;

#pragma GCC diagnostic ignored "-Weffc++"
#pragma GCC diagnostic push
	EEPROMJob(){} // Not initializing due to lack of volatile optimization
#pragma GCC diagnostic pop
	EEPROMJob(reg_t byte, reg_t addr): m_byte{ byte }, m_addr{ addr }
	{}

// Needed for FixedQueue<volatile EEPROMJob>
EEPROMJob(const volatile EEPROMJob&o){m_byte=o.m_byte;m_addr=o.m_addr;}void
operator=(const volatile EEPROMJob&o)volatile{m_byte=o.m_byte;m_addr=o.m_addr;}
private:
	reg_t m_byte;
	reg_t m_addr;
};

// A variable located on the EEPROM
// Stack-only
template <typename T>
class EEPROMVar;

class EEPROM {
public:
	// To be called from ISR(EE_READY_vect)
	static void isr() { 
		if (hasNoMoreJobsToDo()) { m_idle = true; return; }
		handleJob();
	}

	// Read byte from EEPROM at addr
	// Interrupts should be disabled (cli()). Don't call while flash is
	// being programmed.
	static reg_t readByte() {
		read();
		return eedr;
	}

	// Write byte to EEPROM at addr
	// Interrupts should be disabled (cli()). Don't call while flash is
	// being programmed.
	static void writeByte(reg_t byte) {
		setData(byte);
		enableWrite();
		write();
	}

	// Wait until the previous EEPROM write has finished
	static void waitUntilReady() { while (EECR & _BV(EEPE)); }

	// Set EEAR
	static void setAddr(reg_t addr) { eear = addr; }

	// Set EEDR
	static void setData(reg_t data) { eedr = data; }

	// Allow calling write() within 4 clock cycles
	static void enableWrite() { sbis(eecr, EEMPE); }

	// Write byte at addr
	static void write() { sbis(eecr, EEPE); }

	// Read byte at addr
	static void read() { sbis(eecr, EERE); }
private:
	EEPROM() = delete;

	// Whether the EEPROM is currently busy writing
	static inline bool m_idle{ true };

	static inline FixedQueue<volatile EEPROMJob, AVR_EEPROM_QUEUE_SIZE>
		m_jobs{};

	// Add a job to the queue
	static void addJob(const EEPROMJob& job) { m_jobs.push(job); }

	// Handle the job in the front of the queue
	static void handleJob() { m_jobs.pop().handle(); }

	static bool hasNoMoreJobsToDo   () { return m_jobs.empty   (); }
	static bool hasJobsToDo         () { return m_jobs.notEmpty(); }
	static bool hasNoMoreRoomForJobs() { return m_jobs.full    (); }
	static bool hasRoomForJobs      () { return m_jobs.notFull (); }

	template <typename T>
	friend class EEPROMVar;
};

void EEPROMJob::handle() volatile {
	EEPROM::waitUntilReady();
	EEPROM::setAddr(m_addr);
	EEPROM::writeByte(m_byte);
}

// A variable located on the EEPROM
// Stack-only
template <typename T>
class EEPROMVar {
public:
	EEPROMVar() {}
	EEPROMVar(const T& data): m_data{ data }
	{}
	~EEPROMVar() {
		if (sizeof(T) >= s_nextAddr) s_nextAddr -= sizeof(T);
		else s_nextAddr = s_ovfAddr;
	}

	// Add the job of writing this object to the EEPROM to the EEPROM queue
	// Interrupts should be disabled (cli()).
	void save() {
		for (reg_t i{ 0 }; i != sizeof(m_data); ++i)
		{
			if (EEPROM::hasNoMoreRoomForJobs()) EEPROM::handleJob();
			EEPROM::addJob({
				_nthByte(&m_data, i),
				static_cast<reg_t>(m_addr+i)
			});
		}
		if (EEPROM::m_idle)
		{
			EEPROM::m_idle = false;
			EEPROM::handleJob();
		}
	}

	// Read object from EEPROM
	// Potentially waits up to 4ms without (CPU stays running)
	// Assumes interrupts are disabled
	void load() {
		EEPROM::waitUntilReady();
		for (reg_t i{ 0 }; i != sizeof(m_data); ++i)
		{
			EEPROM::setAddr(m_addr+i);
			_nthByte(&m_data, i) = EEPROM::readByte();
		}
	}

	operator T() const { return m_data; }
	constexpr EEPROMVar<T>& operator=(const EEPROMVar<T>& x) = default;
	constexpr auto& operator=(const auto& x) { m_data = x; return *this; }
	constexpr auto operator+(const auto& x) const { return m_data + x; }
	constexpr auto operator-(const auto& x) const { return m_data - x; }
	constexpr auto operator*(const auto& x) const { return m_data * x; }
	constexpr auto operator/(const auto& x) const { return m_data / x; }
	constexpr auto operator%(const auto& x) const { return m_data % x; }
	constexpr auto& operator+=(const auto& x) { m_data += x; return *this; }
	constexpr auto& operator-=(const auto& x) { m_data -= x; return *this; }
	constexpr auto& operator*=(const auto& x) { m_data *= x; return *this; }
	constexpr auto& operator/=(const auto& x) { m_data /= x; return *this; }
	constexpr auto& operator%=(const auto& x) { m_data %= x; return *this; }
	constexpr auto& operator[](auto x) const { return m_data[x]; }
private:
	// Get the nth byte of *ptr
	static inline constexpr const volatile reg_t& _nthByte(
			const volatile auto* ptr, size_t n) {
		return reinterpret_cast<const volatile reg_t*>(ptr)[n];
	}

	// Get the nth byte of *ptr
	static inline constexpr const reg_t& _nthByte(
			const auto* ptr, size_t n) {
		return reinterpret_cast<const reg_t*>(ptr)[n];
	}

	// Get the nth byte of *ptr
	static inline constexpr volatile reg_t& _nthByte(
			volatile auto* ptr, size_t n) {
		return reinterpret_cast<volatile reg_t*>(ptr)[n];
	}

	// Get the nth byte of *ptr
	static inline constexpr reg_t& _nthByte(auto* ptr, size_t n) {
		return reinterpret_cast<reg_t*>(ptr)[n];
	}

	T m_data{};
	reg_t m_addr{ getSafeAddr() };

	constexpr reg_t getSafeAddr() {
		constexpr auto lastEEPROMAddr{ 0xFF };
		if (s_nextAddr > lastEEPROMAddr - sizeof(T)) s_nextAddr = 0x0;
		const auto ret{ s_nextAddr };
		s_nextAddr += sizeof(T);
		return ret;
	}

	static consteval int_fast64_t toInt(char x) { return x - '0'; }

	// Which address the next object should get. Starts with a random 8-bit
	// integer generated at compile time
	static inline uint_fast8_t s_nextAddr{
		[]{
			// __TIME__ contains "HH:MM:SS". Using the entire value
			// instead of just M:SS results in a more uniform (but
			// not perfect) distribution.
			return static_cast<uint_fast8_t>((
				toInt(__TIME__[7])
				+ toInt(__TIME__[6]) * 10
				+ toInt(__TIME__[4]) * 60
				+ toInt(__TIME__[3]) * 600
				+ toInt(__TIME__[1]) * 3600
				+ toInt(__TIME__[0]) * 36000
			) % (UINT8_MAX+1));
		}()
	};

	// At which address an EEPROM overflow happened (see getSafeAddr() and
	// dtor)
	static inline uint_fast8_t s_ovfAddr{};
};

} // namespace avr
#endif // !defined(_AVR_EEPROM_HPP)
