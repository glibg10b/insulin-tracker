#if !defined (_AVR_REGISTER_HPP)
#define       _AVR_REGISTER_HPP

#include "avr/types.hpp"

namespace avr {

// https://cppreference.com/en/cpp/language/template_parameters.html
// > A non-type template parameter must have a structural type, which is one of
// > the following types (optionally cv-qualified, the qualifiers are ignored): 
// >
// > - a literal class type with the following properties: 
// >   - all base classes and non-static data members are public and
// >     non-mutable and
// >   - the types of all base classes and non-static data members are
// >     structural types or (possibly multi-dimensional) array thereof. 

// A reference to a register
class Register {
public:
	using reg_type = reg_t;
	volatile reg_type& reg;
	constexpr auto& operator=(reg_type x) const { reg = x; return *this; }

	constexpr auto operator|(auto x) const { return reg|x; }
	constexpr auto operator&(auto x) const { return reg&x; }
	constexpr auto& operator|=(auto x) const { reg = reg|x; return *this; }
	constexpr auto& operator&=(auto x) const { reg = reg&x; return *this; }
	operator reg_t() const { return reg; }
};

} // namespace avr

#endif // !defined (_AVR_REGISTER_HPP)
