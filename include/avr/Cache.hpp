#if !defined(_AVR_CACHE_HPP)
#define      _AVR_CACHE_HPP

#include "avr/Register.hpp"

#include <avr/io.h>
#include <avr/sfr_defs.h>
#include <stddef.h>
#include <stdint.h>

namespace avr {

// Port register cache
template <const avr::Register& reg>
class Cache {
public:
	static consteval auto& mask() { return m_mask; }
	static consteval auto& store() { return m_store; }
	static constexpr void clear() { m_store = 0; m_mask = 0; }
	static constexpr void flush() {
		if (m_mask) reg = (reg & ~m_mask) | m_store;
	}
private:
	static inline avr::reg_t m_store{};
	static inline avr::reg_t m_mask{};
};

} // namespace avr

#endif // !defined(_AVR_CACHE_HPP)
