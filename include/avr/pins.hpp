#if !defined(_AVR_PINS_HPP)
#define      _AVR_PINS_HPP

#include "avr/Cache.hpp"
#include "avr/Pin.hpp"
#include "avr/device.hpp"
#include "libministdcxx/utility"

#include <stddef.h>

namespace avr {

// Configure pin's data direction (input/output) store
template <const Pin& pin>
constexpr void setDDRCache(Pin::Ddr ddr) {
	Cache<pin.port().ddr()>::mask() |= pin.mask();

	if (ddr == Pin::output) Cache<pin.port().ddr()>::store() |= pin.mask();
}

// Configure pins' data direction (input/output) stores
template <const Pin&... pins>
void setDDRCaches(Pin::Ddr ddr) {
	(setDDRCache<pins>(ddr), ...);
}

//////////////////////// Clear all DDR stores and masks ////////////////////////
template <size_t... Idx>                                                      //
constexpr void _clearDDRCacheHelper(ministd::index_sequence<Idx...> const&) { //
	(Cache<ports[Idx].get().ddr()>::clear(), ...);                        //
}                                                                             //
                                                                              //
constexpr void clearDDRCache() {                                              //
	_clearDDRCacheHelper(ministd::make_index_sequence<portCount>{});      //
}                                                                             //
////////////////////////////////////////////////////////////////////////////////

///////////////////////	Flush all DDR stores using masks ///////////////////////
template <size_t... Idx>                                                      //
constexpr void _flushDDRCacheHelper(                                          //
		ministd::index_sequence<Idx...> const&) {                     //
	(Cache<ports[Idx].get().ddr()>::flush(), ...);                        //
}                                                                             //
                                                                              //
constexpr void flushDDRCache() {                                              //
	_flushDDRCacheHelper(                                                 //
			ministd::make_index_sequence<portCount>{});           //
}                                                                             //
////////////////////////////////////////////////////////////////////////////////

// Configure pins' data directions (input/output)
template <const Pin&... pins>
constexpr void setDDRs(Pin::Ddr ddr) {
	clearDDRCache();
	setDDRCaches<pins...>(ddr);
	flushDDRCache();
}

template <size_t... Idx>
constexpr void _clearAllInputBufferCachesHelper(
		ministd::index_sequence<Idx...> const&) {
	(Cache<bufferedInputs[Idx].get().didr()>::clear(), ...);
}

template <size_t... Idx>
constexpr void _disableAllInputBuffersHelper(
		ministd::index_sequence<Idx...> const&) {
	(sbis(Cache<bufferedInputs[Idx].get().didr()>::store(),
	      bufferedInputs[Idx].get().didbit()), ...);
}

template <size_t... Idx>
constexpr void _flushAllInputBufferCachesHelper(
		ministd::index_sequence<Idx...> const&) {
	(Cache<bufferedInputs[Idx].get().didr()>::flush(), ...);
}

constexpr void disableAllInputBuffers() {
	constexpr auto indices{
		ministd::make_index_sequence<bufferedInputCount>{}
	};
	_clearAllInputBufferCachesHelper(indices);
	_disableAllInputBuffersHelper(indices);
	_flushAllInputBufferCachesHelper(indices);
}

} // namespace avr

#endif // !defined(_AVR_PINS_HPP)

