#if !defined(_AVR_POWER_HPP)
#define      _AVR_POWER_HPP

#include "avr/analog.hpp"
#include "avr/bits.hpp"
#include "avr/device.hpp"
#include "avr/pins.hpp"

namespace avr {

// Enable/disable brown-out detector during certain sleep modes (see datasheet)
constexpr void bodDuringSleep(bool on) { 
	avr::sbis(avr::mcucr, BODSE, BODS);
	if (on) avr::sbis(avr::mcucr, BODS);
	else    avr::cbis(avr::mcucr, BODS);
}

// Disable pull-up resistors
constexpr void disablePullups() { avr::sbis(avr::mcucr, PUD); }

constexpr void disableTWI   () { avr::sbis(avr::prr, PRTWI                 ); }
constexpr void disableTimers() { avr::sbis(avr::prr, PRTIM2, PRTIM1, PRTIM0); }
constexpr void disableSPI   () { avr::sbis(avr::prr, PRSPI                 ); }
constexpr void disableUSART () { avr::sbis(avr::prr, PRUSART0              ); }
constexpr void disableADC   () { avr::sbis(avr::prr, PRADC                 ); }

constexpr void lowCurrentMode() {
	setADCPower(false);
	bodDuringSleep(false);
	disableAllInputBuffers();
	disablePullups();
	disableTWI();
	disableTimers();
	disableSPI();
	disableUSART();
	disableADC();
}

constexpr void enableTimer1() { avr::cbis(avr::prr, PRTIM1); }

} // namespace avr

#endif // !defined(_AVR_POWER_HPP)
