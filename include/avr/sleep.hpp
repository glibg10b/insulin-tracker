#if !defined(_AVR_SLEEP_HPP)
#define      _AVR_SLEEP_HPP

#include "avr/bits.hpp"
#include "avr/device.hpp"

namespace avr {
	namespace sleepMode { enum Mode {
		idle, noise, powerDown, powerSave, standBy, externalStandBy }; }

	constexpr void setSleepMode(sleepMode::Mode mode){
		sbis(smcr, mode + (mode <= sleepMode::powerSave)? 0 : 2);
	}

	inline void sleep() { __asm("sleep"); }
	inline void sleepForever() { while (true) sleep(); }
}

#endif // !defined(_AVR_SLEEP_HPP)
