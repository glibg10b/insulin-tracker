#if !defined (_AVR_REGISTER16_HPP)
#define       _AVR_REGISTER16_HPP

#include "avr/types.hpp"

#define Register Register16
#define reg_t reg16_t

#	ifdef _AVR_REGISTER_HPP
#	undef _AVR_REGISTER_HPP
#	endif
	
#	include "avr/Register.hpp"

#undef reg_t
#undef Register

#endif // !defined (_AVR_REGISTER16_HPP)
