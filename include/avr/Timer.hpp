#if !defined(_AVR_TIMER_CPP)
#define      _AVR_TIMER_CPP

#include "avr/Register.hpp"

namespace avr {

template <typename TReg>
class Timer {
public:
	/** @brief Wave generation mode */
	enum Wgm { normalWgm, phaseCorrect, ctc, fastPWM, phaseAndFrequencyCorrectPWM };

	/** @brief Wave generation top value */
	enum WgTop { icr, ocr, b8, b9, b10, maxTop };

	/** @brief Compare output mode */
	enum Com { none, toggle, normalCOM, inverse };

	/** @brief Clock edge */
	enum Edge { falling, rising };

	/** @brief Clock source */
	enum Cs { stop, cs1, cs8, cs64, cs256, cs1024, pinFalling, pinRising };

	consteval Timer(
			uint_fast8_t ovf,
			uint_fast8_t compA,
			uint_fast8_t compB,
			const TReg& reg,
			const TReg& ocrA,
			const TReg& ocrB,
			const Register& crA,
			const Register& crB,
			const Register* crC=nullptr,
			const TReg* icr=nullptr
		       )
		: m_ovf{ ovf }
		, m_compA{ compA }
		, m_compB{ compB }
		, m_reg{ reg }
		, m_ocrA{ ocrA }
		, m_ocrB{ ocrB }
		, m_crA{ crA }
		, m_crB{ crB }
		, m_crC{ crC }
		, m_icr{ icr }
	{}

	constexpr auto& crA() const { return m_crA; }
	constexpr auto& crB() const { return m_crB; }

	constexpr void setICR(TReg::reg_type val) const { m_icr->reg = val; }
	constexpr void setOCRA(TReg::reg_type val) const { m_ocrA.reg = val; }
	constexpr void setOCRB(TReg::reg_type val) const { m_ocrB.reg = val; }

	template <const Timer<TReg>& timer>
	constexpr void setComAStore(Com) const;

	template <const Timer<TReg>& timer>
	constexpr void setComBStore(Com) const;

	template <const Timer<TReg>& timer>
	constexpr void setWgmStore(Wgm, WgTop) const;

	template <const Timer<TReg>& timer>
	constexpr void setCSStore(Cs cs) const;

	constexpr auto ovfVectNum() { return m_ovf; }
	constexpr auto compAVectNum() { return m_ovf; }
	constexpr auto compBVectNum() { return m_ovf; }
private:
	const uint_fast8_t m_ovf;
	const uint_fast8_t m_compA;
	const uint_fast8_t m_compB;
	const TReg& m_reg;
	const TReg& m_ocrA;
	const TReg& m_ocrB;
	const Register& m_crA;
	const Register& m_crB;
	const Register* const m_crC;
	const TReg* m_icr;
};

} // namespace avr

#endif // !defined(_AVR_TIMER_CPP)
