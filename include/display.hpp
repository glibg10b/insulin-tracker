#if !defined(_DISPLAY_HPP_1656959177)
#define _DISPLAY_HPP_1656959177

#include "avr/device.hpp"
#include "avr/bits.hpp"
#include "libministdcxx/functional"
#include "libministdcxx/utility"

#include <avr/io.h>
#include <avr/sfr_defs.h>
#include <stdint.h>

// Segment pins (top, top left, top right, center, bottom left, bottom right,
// bottom, colon)
#if !defined(DISPLAY_SEGMENTS_PINS)
#define      DISPLAY_SEGMENTS_PINS avr::pB4, avr::pB3, avr::pB0, avr::pD6,     \
	avr::pD2, avr::pD5, avr::pD3, avr::pD4
#endif

// Section pins (not restricted to four)
#if !defined(DISPLAY_SECTIONS_PINS)
#define      DISPLAY_SECTIONS_PINS avr::pB5, avr::pB2, avr::pB1, avr::pD7
#endif

namespace display {

using _flag_type = uint_fast16_t;

template <typename... Params>
consteval uint_fast8_t _countParams(const Params&... params) {
	return sizeof...(params);
}

#define _DISPLAY_SEGMENTS _t, _tl, _tr, _c, _bl, _br, _b, _co
enum Segment { _DISPLAY_SEGMENTS, _maxSegment };
constexpr const ministd::reference_wrapper<const avr::Pin> _pins[]{ DISPLAY_SEGMENTS_PINS, DISPLAY_SECTIONS_PINS };
constexpr auto pinCount{ sizeof(_pins)/sizeof(_pins[0]) };

constexpr uint_fast8_t segmentCount{ _countParams(DISPLAY_SEGMENTS_PINS) };
static_assert(segmentCount == _maxSegment,
		"Incorrect amount of DISPLAY_SEGMENTS_PINS");

template <typename... Segment>
consteval auto _segmentFlags(Segment... segments) {
	return avr::mask<_flag_type>(segments...);
}

constexpr uint_fast8_t sectionCount{ _countParams(DISPLAY_SECTIONS_PINS) };

template <typename... Section>
constexpr auto _sectionFlags(Section... sections) {
	return avr::mask<_flag_type>((sections + _maxSegment)...);
}

constexpr auto _allSegmentFlags { _segmentFlags(_DISPLAY_SEGMENTS) };
constexpr auto _allSectionFlags { []{
	_flag_type ret{};
	for (auto i{ 0 }; i < segmentCount; ++i)
		ret |= avr::mask<_flag_type>(i + _maxSegment);

	return ret;
} };

constexpr _flag_type _toFlags(char letter) {
	switch (letter)
	{
	case '0': return _segmentFlags(_t, _tl, _tr,     _bl, _br, _b);
	case '1': return _segmentFlags(         _tr,          _br);
	case '2': return _segmentFlags(_t,      _tr, _c, _bl,      _b);
	case '3': return _segmentFlags(_t,      _tr, _c,      _br, _b);
	case '4': return _segmentFlags(    _tl, _tr, _c,      _br);
	case '5': return _segmentFlags(_t, _tl,      _c,      _br, _b);
	case '6': return _segmentFlags(_t, _tl,      _c, _bl, _br, _b);
	case '7': return _segmentFlags(_t,      _tr,          _br);
	case '8': return _segmentFlags(_t, _tl, _tr, _c, _bl, _br, _b);
	case '9': return _segmentFlags(_t, _tl, _tr, _c,      _br, _b);
	case ' ':
	default:  return _segmentFlags(                              );
	}
}

template <typename... Section>
constexpr auto _flags(char c, Section... sections) {
	auto ret{ _allSegmentFlags };
	avr::cbyte(ret, _toFlags(c));
	avr::sbyte(ret, _sectionFlags(sections...));
	return ret;
}

// Since loop variables can't be constexpr, we have to resort to these ugly
// helper functions
template <size_t... Idx>
void _clearStoresHelper(ministd::index_sequence<Idx...> const&) {
	( (
	   avr::Cache<_pins[Idx].get().port().reg()>::store() = 0b0,
	   avr::Cache<_pins[Idx].get().port().reg()>::mask() = 0b0
	   ), ...);
}

// Clear the store for each _pins[Idx]'s port
inline void _clearStores() {
	_clearStoresHelper(ministd::make_index_sequence<pinCount>{});
}

inline bool isBitSet(auto x, auto bitPos) { return x & _BV(bitPos); }

template <size_t... Idx>
void _applyFlagsHelper(ministd::index_sequence<Idx...> const&, _flag_type flags)
{
	(
	 (
		avr::sbyte(
		       avr::Cache<_pins[Idx].get().port().reg()>::mask()
		       , _pins[Idx].get().mask()
		),
		isBitSet(flags, Idx)
		? avr::sbyte(
			avr::Cache<_pins[Idx].get().port().reg()>::store()
			, _pins[Idx].get().mask()
		)
		: static_cast<void>(0)
	 ),
	...);
}

// Set the bit for each pin in _pins[] in each port's mask and, if the
// corresponding flag is set, in each port's store
inline void _applyFlags(_flag_type flags) {
	_applyFlagsHelper(ministd::make_index_sequence<pinCount>{}, flags);
}

template <size_t... Idx>
inline void _updatePinsHelper(ministd::index_sequence<Idx...> const&) {
	( (avr::Cache<avr::ports[Idx].get().reg()>::flush()), ...);
}

// Update the pins using the stores
inline void _updatePins() {
	_updatePinsHelper(
		ministd::make_index_sequence<avr::maxPorts>{}
	);
}

inline void _displayFlags(_flag_type flags) {
	_clearStores();
	_applyFlags(flags);
	_updatePins();
}

// Displays letter ['1';'9'] at sections [0;3]
template <typename... Section>
void letter(char c, Section... sections) {
	_displayFlags(_flags(c, sections...));
}

inline void clear() { display::letter('8'); } // 8 results in all pins being low

static char str[sectionCount+1]{};

constexpr auto lastDigit(auto x) { return x % 10ll; }
constexpr auto toChar(auto x) { return x + '0'; }

template <uint_fast8_t N>
constexpr void toStr(char buf[N], auto x) {
	for (uint_fast8_t i{ N-1 }; i-- > 0;)
	{
		buf[i] = toChar(display::lastDigit(x));
		x = x / 10ll;
	}
	buf[N-1] = '\0';
}

void setStr(auto x) { toStr<sectionCount+1>(str, x); }

inline void display() {
	static uint_fast8_t i{ 0 };

	display::letter(str[i], i);
	if (++i >= sectionCount) i = 0;
}

} // namespace display
#endif // !defined(_DISPLAY_HPP_1656959177)
