INCLUDES := -Iinclude -isystem/usr/avr/include
CODE_FILES := $(wildcard *.cpp)
HEADER_FILES := $(wildcard include/*)
HEADER_FILES :=
DEFINES := -DF_CPU=16000000UL -D__STDC_LIMIT_MACROS

DEBUGOPTS := -mmcu=atmega328p -O0 -g -std=c++23 -Wall -Weffc++ -Wextra         \
	-Wsign-conversion --pedantic-errors -lm
CXXOPTS := -mmcu=atmega328p -fno-threadsafe-statics -Ofast -flto -funroll-loops\
	-finline-limit=2147483647 -fmerge-all-constants -fmodulo-sched         \
	-fisolate-erroneous-paths-attribute -fno-math-errno                    \
	-funsafe-math-optimizations -ffinite-math-only -fstdarg-opt            \
	--param max-delay-slot-insn-search=2147483647                          \
	--param max-delay-slot-live-search=2147483647                          \
	--param max-goto-duplication-insns=2147483647                          \
	--param max-inline-insns-single=2147483647                             \
	--param max-inline-insns-auto=2147483647                               \
	--param max-inline-insns-small=2147483647                              \
	--param max-inline-insns-size=2147483647                               \
	--param large-function-growth=2147483647                               \
	--param inline-unit-growth=2147483647                                  \
	--param ipa-cp-unit-growth=2147483647                                  \
	--param large-stack-frame-growth=2147483647                            \
	--param max-inline-insns-recursive=2147483647                          \
	--param max-inline-insns-recursive-auto=2147483647                     \
	--param max-inline-recursive-depth=2147483647                          \
	--param max-inline-recursive-depth-auto=2147483647                     \
	--param early-inlining-insns=2147483647                                \
	--param max-early-inliner-iterations=2147483647                        \
	--param gcse-unrestricted-cost=0                                       \
	--param max-hoist-depth=0                                              \
	--param max-unrolled-insns=2147483647                                  \
	--param max-unroll-times=2147483647                                    \
	--param max-peeled-insns=2147483647                                    \
	--param max-peel-times=2147483647                                      \
	--param max-peel-branches=2147483647                                   \
	--param max-completely-peeled-insns=2147483647                         \
	--param max-completely-peel-times=2147483647                           \
	--param max-completely-peel-loop-nest-depth=2147483647                 \
	--param max-unswitch-insns=2147483647                                  \
	--param max-unswitch-level=2147483647                                  \
	--param iv-consider-all-candidates-bound=2147483647                    \
	--param iv-max-considered-uses=2147483647                              \
	--param dse-max-object-size=65535                                      \
	--param dse-max-alias-queries-per-store=2147483647                     \
	--param scev-max-expr-size=2147483647                                  \
	--param scev-max-expr-complexity=2147483647                            \
	--param max-iterations-to-track=2147483647                             \
	--param stack-clash-protection-guard-size=30                           \
	--param stack-clash-protection-probe-interval=16                       \
	--param max-reload-search-insns=2147483647                             \
	--param max-cselib-memory-locations=2147483647                         \
	--param max-sched-ready-insns=65535                                    \
	--param ssp-buffer-size=65535                                          \
	--param loop-invariant-max-bbs-in-loop=2147483647                      \
	--param ipa-cp-max-recursive-depth=2147483647                          \
	--param jump-table-max-growth-ratio-for-speed=2147483647               \
	--param asan-globals=0                                                 \
	--param asan-stack=0                                                   \
	--param asan-instrument-reads=0                                        \
	--param asan-instrument-writes=0                                       \
	--param analyzer-max-enodes-per-program-point=2147483647               \
	--param analyzer-max-enodes-for-full-dump=2147483647                   \
	--param analyzer-max-recursion-depth=2147483647                        \
	--param analyzer-max-svalue-depth=2147483647                           \
	-std=c++23                                                             \
	-Wall -Weffc++ -Wextra -Wsign-conversion --pedantic-errors             \
	-lm -fuse-linker-plugin -Wl,--gc-sections

default: upload
compile:
	@avr-g++ ${CXXOPTS} ${DEFINES} ${INCLUDES} ${CODE_FILES}               \
		-o a.elf
	@du -bh a.elf

debug:
	@avr-g++ ${DEBUGOPTS} ${DEFINES} ${INCLUDES} ${CODE_FILES} -o a.elf
	@du -bh a.elf

upload: compile
	@avr-objcopy -O ihex -R .eeprom a.elf a.hex
	@avrdude -q -q -F -V -patmega328p -carduino -P/dev/ttyACM0 -b19200     \
		-Uflash:w:a.hex

objdump: compile
	@avr-objdump -D a.elf

clean:
	@rm a.elf a.hex

tags: ${CODE_FILES} ${HEADER_FILES}
	@avr-g++ -M ${CXXOPTS} ${CODE_FILES} ${DEFINES} ${INCLUDES}            \
		| sed -e 's/[\\ ]/\n/g' | sed -e '/^$$/d' -e '/\.o:[ \t]*$$/d' \
		| ctags -L - --c++-kinds=+p --fields=+iaS --extras=+q

.PHONY: vimrc
.ONESHELL:
vimrc: Makefile
	@tee .vimrc > .nvimrc << _EOF_
	set path=./include,/usr/avr/include
	let g:termdebugger='avr-gdb'
	let g:ale_linters = {'cpp': ['g++']}
	let g:ale_cpp_cc_executable='avr-g++'
	let g:ale_cpp_cc_options='${CXXOPTS} ${DEFINES} ${INCLUDES}'
	_EOF_
